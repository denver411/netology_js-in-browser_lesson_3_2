'use strict';

document.addEventListener('DOMNodeInserted', buttonsEvents);

function buttonsEvents() {
  document.querySelector('.items-list').addEventListener('click', pressButton);
}

function pressButton(event) {
  if (event.target.classList.contains('add-to-cart')) {
    addToCart({
      title: event.target.dataset.title,
      price: event.target.dataset.price
    })
  }
}